def my_decorator(func):
    def wrapper():
        print("before the func is called")
        func()
        print("after the func is called")
    return wrapper


def say_whee():
    print("Whee!!!")


say_whee = my_decorator(say_whee)

say_whee()


