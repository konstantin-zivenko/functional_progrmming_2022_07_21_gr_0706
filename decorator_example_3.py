from decorators import do_twice, do_twice_with_args, do_twice_arg_result

@do_twice
def say_whee():
    print("Whee!!!")


@do_twice_with_args
def greet(name):
    print(f"Hello {name}")


@do_twice_arg_result
def return_greeting(name):
    print("Creating greeting")
    return f"Hi {name}!"


say_whee()

greet("Stepan")

print(return_greeting("Maria"))